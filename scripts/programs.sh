#!/bin/bash

basic()
{
# Install wm, shell and basic applications
sudo pacman -S --noconfirm yay
sudo pacman -S --noconfirm chezmoi
sleep .5
# Add my qtile config
sudo pacman -S --noconfirm qtile
sudo pacman -S --noconfirm feh
sudo pacman -S --noconfirm sxhkd
sudo pacman -S --noconfirm picom
# ZSH install and add zsh-plugins
sudo pacman -S --noconfirm zsh
sleep .5
sudo mkdir -p /usr/share/zsh/plugins/ ~/.cache/zsh
touch ~/.cache/zsh/history
git clone https://github.com/zsh-users/zsh-autosuggestions /usr/share/zsh/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git /usr/share/zsh/plugins/zsh-syntax-highlighting
chsh -s $(which zsh)
sh -c "$(curl -fsSL https://starship.rs/install.sh)"
# Browsers
sudo pacman -S --noconfirm brave-browse
sudo pacman -S --noconfirm firefox
yay -S -noconfirm librewolf-bin
# cli - utilities
sudo pacman -S --noconfirm kitty
sudo pacman -S --noconfirm alacritty
sudo pacman -S --noconfirm mpv
sudo pacman -S --noconfirm exa
sudo pacman -S --noconfirm xsel
sudo pacman -S --noconfirm xclip
sudo pacman -S --noconfirm ffmpegthumbnailer
sudo pacman -S --noconfirm w3m
sudo pacman -S --noconfirm tar
sudo pacman -S --noconfirm copyq
sudo pacman -S --noconfirm ranger
sudo pacman -S --noconfirm neovim
sudo pacman -S --noconfirm veracryptc
sudo pacman -S --noconfirm dmenu
sudo pacman -S --noconfirm rofi
sudo pacman -S --noconfirm quodlibet
sudo pacman -S --noconfirm tree
sudo pacman -S --noconfirm git
# Install useful python utilities
sudo pacman -S --noconfirm python3-pip subversion
sudo pacman -S --noconfirm python3
sudo pacman -S --noconfirm subversion
sudo pacman -S --noconfirm python3-pip
yay -S --noconfirm python-pynvim
sudo pacman -S --noconfirm picard
sudo pacman -S --noconfirm qbittorrent
sudo pacman -S --noconfirm bat
sudo pacman -S --noconfirm cmus
sudo pacman -S --noconfirm cava
sudo pacman -S --noconfirm base-devel fftw ncurses alsa-lib iniparser pulseaudio
sudo pacman -S --noconfirm flameshot
sudo pacman -S --noconfirm atool
sudo yay -S --noconfirm pfetch
sudo pacman -S --noconfirm telegram-desktop
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin
# Create some folders needed
mkdir -p ~/.vim/undo
mkdir -p ~/.vim/swaps
mkdir -p ~/.vim/backups
yay -S --noconfirm vscodium
}


art()
{
sudo pacman -S --noconfirm screenkey
sudo pacman -S --noconfirm youtube-dl
sudo pacman -S --noconfirm mpv
sudo pacman -S --noconfirm vlc
sudo pacman -S --noconfirm neofetch
}

fonts()
{
yay -S --noconfirm nerd-fonts-fira-code
yay -S --noconfirm nerd-fonts-hermit
yay -S --noconfirm nerd-fonts-hack
yay -S --noconfirm nerd-fonts-source-code-pro
yay -S --noconfirm ttf-iosevka-nerd
}

# The actual start of the script
echo "First we upgrade all the packages"
sudo pacman -Syu
sleep .5
# Install the basic programs
read -p "Do you want to install the basic utilities? (y/n) " RESP
if [ "$RESP" = "y" ]; then
	basic
else
  echo "You need more bash programming"
fi
# Install the art programs
read -p "Do you want to install some fonts? (y/n) " RESP
if [ "$RESP" = "y" ]; then
	fonts
else
  echo "You need more bash programming"
fi
# Install the art programs
read -p "Do you want to install the basic utilities? (y/n) " RESP
if [ "$RESP" = "y" ]; then
	art
else
  echo "You need more bash programming"
fi

