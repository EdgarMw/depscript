#!/bin/bash

# Install dependencies
sudo pacman -S --noconfirm git wget curl

# Clone the scripts needed
git clone https://gitlab.com/EdgarMw/depscript.git /tmp/jojo

# Install some programs
sh /tmp/jojo/scripts/programs.sh

# Add configs with chezmoi
sh /tmp/jojo/scripts/chezmoi.sh